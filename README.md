# Deloitte Assessment
## REST API for Entity Recognition

## Setting up
```
docker-compose build
docker-compose up
```

The REST API will be accessible on http://0.0.0.0:8000/extract_entity
```
curl -X 'POST' \
  'http://0.0.0.0:8000/extract_entity' \
  -H 'accept: application/json' \
  -H 'Content-Type: multipart/form-data' \
  -F 'file=@FILENAME.json;type=application/json'
```

JSON file format
```
{
    "1": {
        "news": "Lorem ipsum dolor sit amet, ei duo impedit ancillae, et per pericula suscipiantur conclusionemque, et eum quaeque conclusionemque. Choro homero ubique id vel, duo eu velit nonumes, blandit laboramus pri ut. Et mazim commune quo, cu alienum perpetua mei. Id usu saepe recusabo, democritum efficiantur ne eam. Accusam noluisse petentium qui eu.
"
    },
    "2":{
        "news": "Lorem ipsum dolor sit amet, ei duo impedit ancillae, et per pericula suscipiantur conclusionemque, et eum quaeque conclusionemque. Choro homero ubique id vel, duo eu velit nonumes, blandit laboramus pri ut. Et mazim commune quo, cu alienum perpetua mei. Id usu saepe recusabo, democritum efficiantur ne eam. Accusam noluisse petentium qui eu.
"
    }
}
```

## Design
- domain: Consist all the entities and business logic
- entity_recognizer_model: EntityRecognizerModel is an interface, every NER model have to implement it
- repositories: Create and Save news and extracted entity to database
- datasource: Implementation for database

FastAPI is used in this project
PostgreSQL is used as database
from datetime import datetime, timezone

from sqlalchemy import (
    Column,
    String,
    BigInteger,
    Integer,
    TIMESTAMP,
    ForeignKey,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from ..domain.entities.extraction import Extraction

Base = declarative_base()

class NewsTable(Base):
    __tablename__ = "news"

    news_id = Column(BigInteger, primary_key=True)
    source = Column(String)
    key_id = Column(String)
    news_text = Column(String)
    upload_at = Column(TIMESTAMP, default=datetime.now())

    extracted_entity = relationship("ExtractedEntityTable")

    def __init__(self, extraction: Extraction):
        self.source = extraction.source
        self.key_id = extraction.key_id
        self.news_text = extraction.news_text

        self.extracted_entity = [
            ExtractedEntityTable(entity) 
            for entity in extraction.extracted_entity
        ]

class ExtractedEntityTable(Base):
    __tablename__ = "extracted_entity"

    word = Column(String, primary_key=True)
    entity_type = Column(String, primary_key=True)
    news_id = Column(
        BigInteger, 
        ForeignKey("news.news_id"),
        primary_key=True
    )
    total = Column(Integer)

    def __init__(self, extracted_entity):
        self.word = extracted_entity.word
        self.entity_type = extracted_entity.entity_type
        self.total = extracted_entity.total

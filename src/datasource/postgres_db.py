import os
import logging
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class SQLAlchemyClient:
    def __init__(self):
        database_host = os.getenv("POSTGRES_DB_HOST", "localhost")
        database_port = os.getenv("POSTGRES_DB_PORT", 5432)
        database_user = os.getenv("POSTGRES_USER", "postgres")
        database_pass = os.getenv("POSTGRES_PASSWORD", "postgres")
        database_name = os.getenv("POSTGRES_DB", "")
        self.database_uri = (
            f"postgresql+psycopg2://{database_user}:{database_pass}@"
            + f"{database_host}:{database_port}/{database_name}"
        )

        engine = create_engine(self.database_uri)
        self.Session = sessionmaker(bind=engine)

    @contextmanager
    def get_session(self):
        session = self.Session()

        try:
            yield session
            logger.info("Successfully accessed database using SQLAlchemy")

        # Use a bare except since anything going wrong during a write to the database should abort the write
        except Exception as e:
            logger.error(
                f"Error occurred when accessing database using SQLAlchemy. Rolling back...\nException: {e}"
            )
            session.rollback()
        finally:
            session.close()

from .domain.entities.file import NewsCollection
from .domain.entities.extraction import Extraction
from .domain.use_case.entity_recognizer import EntityRecognizer
from .domain.services.entity_recognition_services import EntityRecognitionService

from .entity_recognizer_model.spacy_model import SpacyEntityRecognizerModel
from .repositories.entitiy_repo_impl import EntityRepoImpl
from .datasource.postgres_db import SQLAlchemyClient

from fastapi import FastAPI, File, UploadFile
from fastapi.exceptions import HTTPException
from fastapi.encoders import jsonable_encoder

from typing import List


class EntityRecognitionAPI:
    def __init__(self, entity_recognition_services):
        self.entity_recognition_services = entity_recognition_services

    def __call__(self, file):
        extraction_list = self.entity_recognition_services.process_file(file)

        return extraction_list


app = FastAPI()


entity_recognizer = EntityRecognizer(
    SpacyEntityRecognizerModel(),
)
entity_recognition_service = EntityRecognitionService(
    entity_recognizer,  EntityRepoImpl(SQLAlchemyClient())
)

entity_recognition_api = EntityRecognitionAPI(
    entity_recognition_service
)

@app.post("/extract_entity", response_model=List[Extraction])
def extract_entity(file: UploadFile = File(...)):
    # Only accept json file
    if file.content_type != "application/json":
        raise HTTPException(
            status_code=415,
            detail="Only support json file"
        )


    content = file.file.read()
    file_string = content.decode("utf-8")
  
    news_collection = NewsCollection.from_json(file_string, file.filename)

    result = entity_recognition_api(news_collection)

    return jsonable_encoder(result)

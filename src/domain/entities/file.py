import json
from pydantic.dataclasses import dataclass
from typing import List

from .news import News

@dataclass(frozen=True)
class NewsCollection:
    filename: str
    news: List[News]

    @classmethod
    def from_json(cls, json_string, filename):
        json_dict = json.loads(json_string)

        return cls.from_dict(json_dict, filename)

    @classmethod
    def from_dict(cls, json_dict, filename):
        news = [
            News.from_dict(key_id, text["news"]) 
            for key_id, text in json_dict.items()
        ]
        return cls(
            filename=filename,
            news=news
        )

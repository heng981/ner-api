from pydantic.dataclasses import dataclass
from typing import Tuple, List

@dataclass(frozen=True)
class News:
    key_id: str
    text: str

    @classmethod
    def from_dict(cls, key_id, text):
        return cls(key_id, text)
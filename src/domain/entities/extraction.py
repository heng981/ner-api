from pydantic.dataclasses import dataclass
from typing import Tuple, List

@dataclass(frozen=True)
class ExtractedEntity:
    word: str
    entity_type: str # Enum ?
    total: int


@dataclass(frozen=True)
class Extraction:
    news_text: str
    source: str
    key_id: str
    extracted_entity: List[ExtractedEntity]
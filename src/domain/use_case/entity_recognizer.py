from typing import List

from ..entities.file import NewsCollection
from ..entities.news import News
from ..entities.extraction import ExtractedEntity, Extraction

class EntityRecognizer:
    def __init__(self, model):
        self.model = model

    def extract_entity_from_file(self, news_collection: NewsCollection) -> List[Extraction]:
        extraction_list = []
        for news in news_collection.news:
            extracted_entity = self._process_one_news(news)
            extraction_list.append(
                Extraction(
                    news_text=news.text,
                    key_id=news.key_id,
                    source=news_collection.filename,
                    extracted_entity=extracted_entity
                )
            )

        return extraction_list

    def _process_one_news(self, news: News) -> List[ExtractedEntity]:
        extracted_entity = self.model.extract_entity(news)

        return extracted_entity

from ..use_case.entity_recognizer import EntityRecognizer
from ..repositories.entitiy_repo import EntityRepo

class EntityRecognitionService:
    def __init__(
        self, 
        entitiy_recognizer: EntityRecognizer,
        entity_repo: EntityRepo
    ):
        self.entity_recognizer = entitiy_recognizer
        self.entity_repo = entity_repo

    def process_file(self, file):
        # Extract the named entity from all the news in the file
        extraction_list = self.entity_recognizer.extract_entity_from_file(file)

        # Save it to database
        for extraction in extraction_list:
            self.entity_repo.save(extraction)

        return extraction_list

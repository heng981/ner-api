from abc import abstractmethod

from ..entities.extraction import Extraction

class EntityRepo:
    @abstractmethod
    def save(self, extraction: Extraction):
        raise NotImplementedError

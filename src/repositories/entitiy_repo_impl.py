from ..datasource.tables import NewsTable
from ..domain.entities.extraction import Extraction
from ..domain.repositories.entitiy_repo import EntityRepo

class EntityRepoImpl(EntityRepo):
    def __init__(self, database):
        self.database = database

    def save(self, extraction: Extraction):
        with self.database.get_session() as session:
            entry = NewsTable(extraction)
            session.add(entry)
            session.commit()

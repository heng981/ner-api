import spacy

from ..domain.entities.news import News
from ..domain.entities.extraction import ExtractedEntity
from .entity_recognizer_model import EntityRecognizerModel

from collections import defaultdict
from typing import List

nlp = spacy.load("en_core_web_sm")


class SpacyEntityRecognizerModel(EntityRecognizerModel):
    def extract_entity(self, news: News) -> List[ExtractedEntity]:
        doc = nlp(news.text)
        extracted_entity = []

        entity_dict = defaultdict(int)
        # Count for each entity
        # Entity with same string might have different type
        for entity in doc.ents:
            entity_dict[(entity.text, entity.label_)] += 1

        for entity, total in entity_dict.items():
            extracted_entity.append(
                ExtractedEntity(
                    word=entity[0],
                    entity_type=entity[1],
                    total=total
                )
            )

        return extracted_entity

from ..domain.entities.news import News
from ..domain.entities.extraction import ExtractedEntity

from typing import List
from abc import abstractmethod

class EntityRecognizerModel:
    @abstractmethod
    def extract_entity(self, news: News) -> List[ExtractedEntity]:
        raise NotImplementedError

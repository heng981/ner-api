FROM python:3.7-slim


# RUN apt-get update && apt-get install -y --no-install-recommends python3-dev python-dev build-essential
RUN pip install --upgrade pip
COPY ./requirements.txt /app/requirements.txt
COPY src/ /app

RUN pip install -r /app/requirements.txt

RUN python -m spacy download en_core_web_sm

EXPOSE 8000

CMD ["uvicorn", "app.app:app", "--host", "0.0.0.0", "--port", "8000"]
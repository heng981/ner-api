CREATE TABLE news
(
    news_id     BIGSERIAL       PRIMARY KEY NOT NULL,
    source      VARCHAR(50)                 NOT NULL,
    key_id      VARCHAR(20)                 NOT NULL,
    news_text   TEXT                        NOT NULL,
    upload_at   TIMESTAMP                   NOT NULL DEFAULT CURRENT_TIMESTAMP   
);

CREATE TABLE extracted_entity
(
    word        TEXT                        NOT NULL,
    entity_type VARCHAR(20)                 NOT NULL,
    news_id     BIGINT                      NOT NULL,
    total       INT                         NOT NULL DEFAULT 1,
    PRIMARY KEY (news_id, word, entity_type),
    FOREIGN KEY (news_id) REFERENCES News (news_id)
)